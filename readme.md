# README - Automerge Testing

The following are steps to test the ability to inform git that certain filenames 
such as pom.xml should always use a custom "ours" merge driver so that merge conflicts 
for the designated file(s) will always use the version from HEAD instead of the incoming 
merge ref.

Discussed in the Atlassian Community Post:
https://community.atlassian.com/t5/Bitbucket-questions/Prevent-automatic-merging-for-specific-file-upcoming-release/qaq-p/692840?utm_source=atlcomm&utm_medium=email&utm_campaign=immediate_general_answer&utm_content=topic


1. Created a .gitconfig file in the repo root with the following contents: 

```
[merge "ours"]
  driver = true
```

2. Created pom.xml file

3. Created a .gitattributes file in the repo root as follows: 

```
pom.xml merge=ours
```
4. Create a release branch

```
git checkout -b release/1.1
```
5. create a feature file 1
```
echo My Awesome Feature 1 > feature1.txt
```
6. Update pom.xml to version 1.1.0
7. Add/Commit feature1.txt and pom.xml
8. Create a future release branch that will have concurrent developement with release/1.1

```
git checkout -b release/1.2
```
9.  create a feature file 2
```
echo My Awesome Feature 2 > feature2.txt
```
10. Update the version in pom.xml to 1.2.0
11. Add/Commit feature2.txt and pom.xml
12. Checkout release/1.1 and make a bugfix to the feature1 file
```
git checkout release/1.1
echo Feature1 fix >> feature1.txt
```
13.  Update pom to 1.1.1 and commit change.
14.  Push release/1.1 and release/1.2 to Bitbucket
14.  Create a PR from release/1.1 to 1.2
15.  See message: 
```
This pull request can't be merged.

You will need to resolve conflicts to be able to merge. More information.
```
16. Do the merge locally and see that the local merge chose the pom.xml from the release/1.2 branch
```
$ git checkout -b release/1.2-merged
Switched to a new branch 'release/1.2-merged'

$ git merge release/1.1
Merge made by the 'recursive' strategy.
 feature1.txt | 1 +
 1 file changed, 1 insertion(+)
```


